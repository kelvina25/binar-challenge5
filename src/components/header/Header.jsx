import React, { useRef, useEffect, useContext } from 'react';
import { AuthContext } from "./../../context/AuthContext";
import { Link, useLocation, useNavigate } from 'react-router-dom';
import MenuLink from "../menuLink/MenuLink";
import { ExitToAppOutlined } from "@mui/icons-material";
import './header.scss';

import logo from '../../assets/tmovie.png';


const headerNav = [
    {
        display: 'Home',
        path: '/home'
    },
    {
        display: 'Movies',
        path: '/movie'
    },
    {
        display: 'TV Series',
        path: '/tv'
    }
];

const Header = () => {
    const { dispatch } = useContext(AuthContext);
    const navigate = useNavigate();

    const { pathname } = useLocation();
    const headerRef = useRef(null);
    const active = headerNav.findIndex(e => e.path === pathname);
    const handleLogout = (e) => {
        dispatch({ type: "LOGOUT" });
        navigate("/login");
    };

    useEffect(() => {
        const shrinkHeader = () => {
            if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
                headerRef.current.classList.add('shrink');
            } else {
                headerRef.current.classList.remove('shrink');
            }
        }
        window.addEventListener('scroll', shrinkHeader);
        return () => {
            window.removeEventListener('scroll', shrinkHeader);
        };
    }, []);

    return (
        <div ref={headerRef} className="header">
            <div className="header__wrap container">
                <div className="logo">
                    <img src={logo} alt="" />
                    <Link to="/">MovieList</Link>
                </div>
                <ul className="header__nav">
                    {
                        headerNav.map((e, i) => (
                            <li key={i} className={`${i === active ? 'active' : ''}`}>
                                <Link to={e.path}>
                                    {e.display}
                                </Link>
                            </li>
                        ))
                    }
                    <span onClick={handleLogout}>
                        <MenuLink  text="Logout" />
                    </span>
                </ul>
            </div>
        </div>
    );
}






export default Header;