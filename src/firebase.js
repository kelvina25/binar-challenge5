import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, GithubAuthProvider } from "firebase/auth";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyA01RY_CZoWNianCyHsqXXOMQeEZOjaN3c",
  authDomain: "binar-reactjs-loginregister.firebaseapp.com",
  projectId: "binar-reactjs-loginregister",
  storageBucket: "binar-reactjs-loginregister.appspot.com",
  messagingSenderId: "1862466235",
  appId: "1:1862466235:web:b43343cd06d677e7b7c79e",
  measurementId: "G-7EXYHY2L31"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const storage = getStorage();

export const provider = new GoogleAuthProvider();
export const provider2 = new GithubAuthProvider();
export default app;
