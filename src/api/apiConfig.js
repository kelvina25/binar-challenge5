const apiconfig = {
    baseUrl : 'https://api.themoviedb.org/3/',
    apiKey: 'a217ef277611b78c9824a4dfc0a1500c',
    originalImage : (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
    w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`
}

export default apiconfig