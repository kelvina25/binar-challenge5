import React from "react";
import { useContext } from "react";
import { AuthContext } from "./context/AuthContext";
// import 'swiper/dist/js/swiper.js'
import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import "./App.scss";


import {  createBrowserRouter, Outlet, RouterProvider, Navigate } from "react-router-dom";

import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Catalog from "./pages/Catalog";
import Home from "./pages/Home";
import Detail from "./pages/detail/Detail";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";

import Routing from "./config/Routing";



function App() {
  const { currentUser } = useContext(AuthContext);

  const AuthRoute = ({ children }) => {
    return currentUser ? children : <Navigate to="/login" />;
  };

  const Layout = () => {
    return (
      <>
        <Header />
        <Outlet />
        <Footer />
      </>

      
    );
  };
  
  const router = createBrowserRouter([
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/register",
      element: <Register />,
    },
    {
      path: "/",
      element: (
        <AuthRoute>
          <Layout />
        </AuthRoute>
      ) ,
      children: [
        
        {
          path: "/",
          element: 
            <Home />

        },

        {
          path: "home",
          element: <Home />,
        },
        
        
        {
          path: '/:category',
          element: <Catalog />,
        },

        {
          path: '/:category/:id',
          element: <Detail />
        },

        {
          path: '/:category/search/:keyword',
          element: <Catalog />
        }
      ],  
    },
  ]);
  return (
    <div>
      <RouterProvider router={router} />
      
    </div>

);
}


export default App;
